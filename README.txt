Blender-BFCT
============

The aim of this web application is to facilitate the management of Blender Foundation Certified Trainer applications. Aspiring trainers can submit one application, that will then be reviewed by the BFCT board and eventually approved.

The applicant has a dashboard where he can follow the review procedure.
