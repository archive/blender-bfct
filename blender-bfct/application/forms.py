from flask_wtf import Form
from wtforms import TextField
from wtforms import TextAreaField
from wtforms import BooleanField
from wtforms import SelectMultipleField
from wtforms import SelectField

from wtforms.validators import DataRequired
from wtforms.fields.html5 import URLField
from wtforms.validators import url

class ApplicationForm(Form):
    network_profile = TextField('Blender Network Profile')
    website = URLField(validators=[url(), DataRequired()])
    city_country = TextField('City and Country', validators=[DataRequired()])
    teaching = BooleanField('Teaching')
    institution_name = TextField('School or institution name')
    skills = SelectMultipleField('Areas of expertise', coerce=int)
    video_example = URLField(validators=[url(), DataRequired()])
    written_example = URLField(validators=[url(), DataRequired()])
    portfolio_cv = URLField(validators=[url(), DataRequired()])
    bio_message = TextAreaField('Your message for the board', validators=[DataRequired()])

class CommentForm(Form):
    text = TextAreaField('Your comment', validators=[DataRequired()])

class ApplicationStatusForm(Form):
    status = SelectField('Status',
        choices=[
        ('submitted', 'Submitted'),
        ('under_review', 'Under Review'),
        ('pending', 'Pending subscription'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected')],
        id="set_status")
