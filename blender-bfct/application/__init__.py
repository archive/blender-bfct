from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.mail import Mail

# Create app
app = Flask(__name__)
import config
app.config.from_object(config.Development)

# Create database connection object
db = SQLAlchemy(app)
mail = Mail(app)

from controllers import main
from controllers import admin
from controllers.applications import applications

app.register_blueprint(applications, url_prefix='/applications')
