import datetime
from application import app, db
from application.helpers import pretty_date
from users import User

from sqlalchemy.ext.associationproxy import association_proxy
from flask.ext.security import current_user


class Application(db.Model):
    __table_args__ = {'schema': 'blender-bfct'}
    id = db.Column(db.Integer(), primary_key=True)
    #blender_id = db.Column(db.Integer(), db.ForeignKey(User.id), nullable=False)
    #user = db.relationship('User')
    blender_id = db.Column(db.Integer(), nullable=False)
    network_profile = db.Column(db.String(255))
    website = db.Column(db.String(255))
    city_country = db.Column(db.String(255))
    institution_name = db.Column(db.String(255))
    skills = db.relationship('Skill', secondary='skills_applications',
                            backref=db.backref('applications', lazy='dynamic', cascade='all'))
    video_example = db.Column(db.String(255))
    written_example = db.Column(db.String(255))
    portfolio_cv = db.Column(db.String(255))
    bio_message = db.Column(db.Text())
    submission_date = db.Column(db.DateTime(), default=datetime.datetime.now)
    status = db.Column(db.String(255))
    approve = db.Column(db.Integer(), default=0)
    reject = db.Column(db.Integer(), default=0)
    review_start_date = db.Column(db.DateTime())
    review_end_date = db.Column(db.DateTime())
    renewal_date = db.Column(db.DateTime())

    #reviewers = db.relationship('ReviewersApplications', backref=db.backref('reviewers', cascade='all'))

    def show_pretty_date(self, stage_date):
        if stage_date == 'submission':
            return pretty_date(self.submission_date)
        elif stage_date == 'review_start':
            return pretty_date(self.review_start_date)
        elif stage_date == 'review_end':
            return pretty_date(self.review_end_date)
        else:
            return '--'

    def is_reviewed(self):       
        for review in ReviewersApplications.query.\
            filter_by(application_id=self.id).\
            all():
            if review.reviewer_blender_id == current_user.id:
                return True
        return False

    @property
    def user(self):
        return User.query.get_or_404(self.blender_id)



class Skill(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))
    
    def __str__(self):
        return self.name



skills_applications = db.Table('skills_applications',
        db.Column('application_id', db.Integer(), db.ForeignKey(Application.id)),
        db.Column('skill_id', db.Integer(), db.ForeignKey('skill.id')))


class ReviewersApplications(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    application_id = db.Column(db.Integer(), db.ForeignKey(Application.id), nullable=False)
    application = db.relationship('Application', backref=db.backref('reviewers', cascade='all'))
    #reviewer_blender_id = db.Column(db.Integer(), db.ForeignKey(User.id), nullable=False)
    #reviewer = db.relationship('User')
    reviewer_blender_id = db.Column(db.Integer(), nullable=False)
    approved = db.Column(db.Boolean(), nullable=False)

    @property
    def reviewer(self):
        return User.query.get_or_404(self.reviewer_blender_id)


class Comment(db.Model):
    """Comments to an application. Only BFCT board members can add comments
    via the applications review inferface"""

    id = db.Column(db.Integer(), primary_key=True)

    application_id = db.Column(db.Integer(), db.ForeignKey(Application.id), nullable=False)
    application = db.relationship('Application', backref=db.backref('comments', cascade='all'))

    blender_id = db.Column(db.Integer(), nullable=False)
    text = db.Column(db.Text())
    creation_date = db.Column(db.DateTime(), default=datetime.datetime.now)


    @property
    def pretty_creation_date(self):
        return pretty_date(self.creation_date)

    @property
    def user(self):
        return User.query.get_or_404(self.blender_id)
