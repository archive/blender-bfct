from flask.ext.security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin

from application import app
from application import db


class User(db.Model, UserMixin):
    __bind_key__ = 'users'
    id = db.Column(db.Integer(), primary_key=True)
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary='roles_users',
                            backref=db.backref('users', lazy='dynamic'))

    def __unicode__(self):
        return unicode(self.email) or u''


class Role(db.Model, RoleMixin):
    __bind_key__ = 'users'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

# Define models
roles_users = db.Table('roles_users',
    db.Column('user_id', db.Integer(), db.ForeignKey(User.id)),
    db.Column('role_id', db.Integer(), db.ForeignKey(Role.id)),
    info={'bind_key': 'users'})

# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)
