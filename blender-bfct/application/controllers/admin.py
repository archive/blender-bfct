from application import app, db
from application.models.users import user_datastore, User
from application.models.applications import Application

from flask import render_template, redirect, url_for
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext import admin, login
from flask.ext.admin import Admin, expose, form
from flask.ext.admin.contrib import sqla
from flask.ext.admin.contrib.sqla import ModelView
from flask.ext.admin.base import BaseView
from flask.ext.security import login_required, current_user, roles_accepted

from werkzeug import secure_filename
from jinja2 import Markup
import os, hashlib, time
import os.path as op


# Create customized views with access restriction
class CustomModelView(ModelView):
    def is_accessible(self):
        default_role = user_datastore.find_role("admin")
        #return login.current_user.is_authenticated()
        return login.current_user.has_role(default_role)

class CustomBaseView(BaseView):
    def is_accessible(self):
        return True


# Create customized index view class that handles login & registration
class MyAdminIndexView(admin.AdminIndexView):

    @expose('/')
    def index(self):
        default_role = user_datastore.find_role("admin")
        if login.current_user.is_authenticated() and login.current_user.has_role(default_role):
            return super(MyAdminIndexView, self).index()
        else:
            return redirect(url_for('homepage'))

    @expose('/logout/')
    def logout_view(self):
        login.logout_user()
        return redirect(url_for('homepage'))


class ApplicationView(CustomModelView):
    column_list = ('user.first_name', 'user.last_name', 'city_country', 'submission_date', 'status')
    column_labels = {'user.first_name' : 'First Name', 'user.last_name' : 'Last Name'}
    column_searchable_list = ('website', 'status')
    can_create = False
    # form_ajax_refs = {
    #     'user': {
    #         'fields': (User.email, User.first_name, User.id)
    #     }
    # }


# Create admin
backend = admin.Admin(
    app, 
    'BFCT management', 
    index_view=MyAdminIndexView(), 
    base_template='admin/layout_admin.html'
)

backend.add_view(ApplicationView(Application, db.session, name='Applications', url='applications'))
