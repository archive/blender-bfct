import datetime
from dateutil.relativedelta import relativedelta
from sqlalchemy import desc

from application import db
from application.forms  import CommentForm
from application.forms  import ApplicationStatusForm
from application.models.users import *
from application.models.applications import Application, Skill, ReviewersApplications, Comment

from flask import render_template, redirect, url_for, request, Blueprint, abort
from flask.ext.security import login_required, roles_accepted
from flask.ext.security.core import current_user



applications = Blueprint('applications', __name__)

@applications.route('/')
@roles_accepted('bfct_board', 'bfct_manager', 'admin')
def index():
    return render_template('applications/index.html', 
        title='applications',
        applications=Application.query.order_by(desc('submission_date')).all())

@applications.route('/view/<int:id>')
@login_required
@roles_accepted('bfct_board', 'bfct_manager', 'admin')
def view(id):
    application = Application.query.get_or_404(id)
    comment_form = CommentForm()
    application_status_form = ApplicationStatusForm(
        status=application.status)

    review = ReviewersApplications.query.\
        filter_by(application_id=id).\
        filter_by(reviewer_blender_id=current_user.id).\
        first()

    reviews = ReviewersApplications.query.\
        filter_by(application_id=id).\
        all()

    return render_template('applications/view.html', 
        title='applications',
        application=application,
        review=review,
        reviews=reviews,
        comment_form=comment_form,
        application_status_form=application_status_form)

@applications.route('/vote/<int:approved>/<int:id>')
@roles_accepted('bfct_board', 'bfct_manager', 'admin')
def vote(approved, id):
    application = Application.query.get_or_404(id)
    review = ReviewersApplications.query.\
        filter_by(application_id=id).\
        filter_by(reviewer_blender_id=current_user.id).\
        first()

    if approved:
        if review:
            application.reject -= 1
        application.approve += 1
    else:
        if review:
            application.approve -= 1
        application.reject += 1

    if application.status == 'submitted':
        application.status = 'under_review'
        application.review_start_date = datetime.datetime.now()
    db.session.add(application)

    if not review:
        review = ReviewersApplications(
            application_id=id,
            reviewer_blender_id=current_user.id,
            approved=approved) 
    else:
        review.approved = approved

    db.session.add(review)
    
    db.session.commit()
    return redirect(url_for('.view', id=id))

@applications.route('/final-review/<int:approved>/<int:id>')
@roles_accepted('bfct_manager', 'admin')
def final_review(approved, id):
    application = Application.query.get_or_404(id)
    if application.status in ['under_review', 'pending']:
        if approved:
            application.status = 'approved'
        else:
            application.status = 'rejected'
        review_end_date = datetime.datetime.now()
        application.review_end_date = review_end_date
        application.renewal_date = review_end_date + relativedelta(years=1)
        db.session.add(application)
   
        db.session.commit()
    else:
        return abort(404)
    return redirect(url_for('.view', id=id))


@applications.route('/comment/<int:application_id>', methods=['POST'])
@roles_accepted('bfct_board', 'bfct_manager', 'admin')
def comment(application_id):
    comment_form = CommentForm()
    if comment_form.validate_on_submit():
        comment = Comment(
            blender_id=current_user.id,
            application_id=application_id,
            text=comment_form.text.data)

        db.session.add(comment)
        db.session.commit()
    return redirect(url_for('.view', id=application_id))


@applications.route('/edit/<int:application_id>/update-bfct-role/')
@roles_accepted('bfct_manager', 'admin')
def update_bfct_role(application_id):
    application = Application.query.get_or_404(application_id)
    user = User.query.get_or_404(application.blender_id)
    applications = Application.query\
        .filter_by(blender_id=user.id)\
        .all()

    for application in applications:
        if application.status == 'approved':
            # TODO: additional check to see if application is valid
            user_datastore.add_role_to_user(user, 'bfct_trainer')
            db.session.commit()
        else:
            user_datastore.remove_role_from_user(user, 'bfct_trainer')
            db.session.commit()
    return redirect(url_for('.view', id=application_id))


@applications.route('/edit/<int:application_id>/status/<status>')
@roles_accepted('bfct_manager', 'admin')
def set_status(application_id, status):
    application = Application.query.get_or_404(application_id)
    application.status = status

    db.session.commit()
    return 'ok'
