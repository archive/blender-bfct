from application import app, db
from application.forms import ApplicationForm
from application.emails import send_email_notification_new_submission
from application.models.users import *
from application.models.applications import Application, Skill

from flask import render_template, redirect, url_for, request, flash
from flask.ext.security import login_required
from flask.ext.security.core import current_user
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound


# Views
@app.route('/')
def homepage():
    latest_application = None
    if current_user.is_authenticated():
        applications = Application.query\
            .filter_by(blender_id=current_user.id)\
            .all()
        for application in applications:
            if application.status != 'rejected':
                latest_application = application
                break

    return render_template('index.html', application=latest_application, title='home')

@app.route('/become-a-trainer/')
def become_a_trainer():
    return render_template('become_a_trainer.html', title='become_a_trainer')


@app.route('/apply', methods=['GET', 'POST'])
@login_required
def apply():
    application = Application.query.filter_by(blender_id=current_user.id).first()
    if application:
        return redirect(url_for('my_application'))
    else:
        form = ApplicationForm()
        form.skills.choices = [(s.id, s.name) for s in Skill.query.all()]
        if form.validate_on_submit():
            application = Application(
                blender_id=current_user.id,
                network_profile=form.website.data,
                website=form.website.data,
                city_country=form.city_country.data,
                institution_name=form.institution_name.data,
                video_example=form.video_example.data,
                written_example=form.written_example.data,
                portfolio_cv=form.portfolio_cv.data,
                bio_message=form.bio_message.data,
                status='submitted')
            for skill in form.skills.data:
                s = Skill.query.get(skill)
                application.skills.append(s)
            db.session.add(application)
            db.session.commit()
            send_email_notification_new_submission(application)
            return redirect(url_for('my_application'))
        # print form.errors
        return render_template('apply.html', 
            title='apply',
            form=form)


@app.route('/my-application')
@login_required
def my_application():
    try:
        application = Application.query.filter_by(blender_id=current_user.id).one()
    except MultipleResultsFound:
        flash('You have submitted more than one application. Get in touch with support@blendernetwork.org.')
        return render_template('index.html')
    except NoResultFound:
        return redirect(url_for('apply'))

    return render_template('my_application.html',
        application=application)
