from flask import render_template
from flask.ext.mail import Message
from application import mail
from application import app
from decorators import async

from application.models.users import user_datastore
from application.helpers import get_board_members_emails

from flask.ext.security import current_user


@async
def send_async_email(msg):
    with app.app_context():
       mail.send(msg)


def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender = sender, recipients = recipients)
    msg.body = text_body
    msg.html = html_body
    send_async_email(msg)


def send_email_notification_new_submission(application):
    board_members = get_board_members_emails()
    if application.user.first_name:
        applicant = application.user.first_name + " " + application.user.last_name
    else:
        applicant = application.user.email
    send_email("[BFCT] " + applicant + " applied for BFCT",
        'bfct-robot@blender.org',
        board_members,
        render_template("email/new_submission.txt",
            application=application),
        render_template("email/new_submission.html",
            application=application))
